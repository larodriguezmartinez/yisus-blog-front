import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../models/post';
import { global } from './global';
import { UserService } from './user.service';

@Injectable()
export class PostService{

   public url: string;
   public id;
   public user_id;
   public title: string;
   public subtitle: string;
   public content: string;
   public created_at: any;
   public token;

   constructor(
      public _http: HttpClient,
      private _userService: UserService,
   ){
      this.url = global.url
   }

   test(){
      return "Servicio posts";
   }

   getPosts(): Observable<any>{

      this.token = this._userService.getToken();

      let headers = new HttpHeaders().set('Authorization', 'Bearer '+this.token );

      return this._http.get(this.url+'post', { headers: headers });
   }

   getPost(id): Observable<any>{

      this.token = this._userService.getToken();

      let headers = new HttpHeaders().set('Authorization', 'Bearer '+this.token );

      return this._http.get(this.url+'post/' + id, { headers: headers });
   }

   setPost(post): Observable<any>{

      let json = JSON.stringify(post);

      this.token = this._userService.getToken();

      let headers = new HttpHeaders().set('Authorization', 'Bearer '+this.token );

      return this._http.post(this.url+'post', json, { headers: headers });
   }
}
