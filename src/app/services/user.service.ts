import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';
import { global } from './global';

@Injectable()
export class UserService{

   public url: string;
   public identity;
   public token;

   constructor(
      public _http: HttpClient
   ){
      this.url = global.url
   }

   test(){
      return "Servicio usuario";
   }

   register(user): Observable<any>{
      let json = JSON.stringify(user);

      let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

      return this._http.post(this.url+'register', json, { headers: headers });
   }

   singup(user, gettoken = null): Observable<any>{

      if(gettoken != null){
         user.gettoken = 'true';
      }

      let json = JSON.stringify(user);

      let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');

      return this._http.post(this.url+'login', json, { headers: headers });

   }

   getIdentity(){

      let identity = JSON.parse(localStorage.getItem('identity'));

      if( identity && identity != 'undefined'){
         this.identity = identity;
      }else{
         this.identity = null;
      }

      return this.identity;
   }

   getToken(){

      let token = localStorage.getItem('token');

      if( token && token != 'undefined'){
         this.token = token;
      }else{
         this.token = null;
      }

      return this.token;
   }
}

