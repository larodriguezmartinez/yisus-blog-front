import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { PostsComponent } from './components/posts/posts.component';
import { PostDetailComponent } from './components/post-detail/post-detail.component';
import { PostNewComponent } from './components/post-new/post-new.component';
import { HomeComponent } from './components/home/home.component';
import { ErrorComponent } from './components/error/error.component';

import { IdentityGuard } from './services/identity.guard';

const appRoutes: Routes = [
   { path: '', component: HomeComponent },
   { path: 'inicio', component: HomeComponent },
   { path: 'login', component: LoginComponent },
   { path: 'logout/:sure', component: LoginComponent },
   { path: 'registro', component: RegisterComponent },
   { path: 'posts', component: PostsComponent , canActivate: [IdentityGuard] },
   { path: 'post/:id', component: PostDetailComponent , canActivate: [IdentityGuard] },
   { path: 'new-post', component: PostNewComponent , canActivate: [IdentityGuard] },
   { path: 'error', component: ErrorComponent },
   { path: '**', component: ErrorComponent }
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);
