export class User{
   constructor(
      public id: number,
      public name: string,
      public surname: string,
      public email: string,
      public birthdate: any,
      public legal: any,
      public avatar: string,
      public profile_id: number,
      public password: string,
   ){}
}

