export class Post{
   constructor(
      public id: number,
      public user_id: number,
      public title: string,
      public subtitle: string,
      public content: string,
      public created_at: any,
      public images: any,
   ){}
}
