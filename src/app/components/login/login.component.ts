import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [ UserService ],
})
export class LoginComponent implements OnInit {

  public pageTitle: string;
  public user: User;
  public status: string;
  public token;
  public identity;

  constructor(
    private _userService: UserService,
    private _router: Router,
    private _route: ActivatedRoute,
  ) {

    this.pageTitle = 'Login';
    this.user = new User(null, '', '', '', '', '', 'avatar.png', 2, '');
  }

  ngOnInit() {
    this.logout();
    console.log('%cInit LOGIN component', 'color: #bada55');
  }

  onSubmit(form){
    this._userService.singup(this.user).subscribe(
      response =>  {
        this.status = response.success.message;

        if(this.status == 'Login correcto'){

          this.token = response.success.token;
          this.identity = response.success.user;

          localStorage.setItem('token', this.token);
          localStorage.setItem('identity', JSON.stringify(this.identity));

          this._router.navigate(['inicio']);

        }
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  logout(){

    this._route.params.subscribe(params => {

      let logout = +params['sure'];

      if(logout == 1){

        localStorage.removeItem('identity');
        localStorage.removeItem('token');

        this.token = null;
        this.identity = null;

        this._router.navigate(['inicio']);
      }

    });
  }
}
