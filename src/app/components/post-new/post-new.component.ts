import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { PostService } from '../../services/posts.service';
import { UserService } from '../../services/user.service';
import { global } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
   selector: 'app-post-new',
   templateUrl: './post-new.component.html',
   styleUrls: ['./post-new.component.css'],
   providers: [ PostService ]
})
export class PostNewComponent implements OnInit {

   public pageTitle: string;
   public post: Post;
   public token;
   public identity;
   public status: string;
   public id;


   public froala_options: Object = {

      charCounterCount:true,
      quickInsertEnabled: false,
      height: 200,
      placeholderText: 'Contenido',
      buttonsVisible: 8,
      htmlAllowedTags: ['p', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6'],
      toolbarButtons:  ['bold','italic','underline','paragraphFormat','alert'],
      toolbarButtonsXS: ['bold','italic','underline','paragraphFormat','alert'],
      toolbarButtonsSM: ['bold','italic','underline','paragraphFormat','alert'],
      toolbarButtonsMD: ['bold','italic','underline','paragraphFormat','alert'],
   }
   constructor(

      private _postService: PostService,
      private _userService: UserService,
      private _route: ActivatedRoute,
      private _router: Router
   ) {
      this.pageTitle = "Post create";
      this.token = this._userService.getToken();
      this.identity = this._userService.getIdentity();

   }
   ngOnInit() {
      console.log('%cInit POST CREAR component', 'color: #bada55');
      this.post = new Post(null,this.identity.id,'','','',null,null);
   }

   onSubmit(form){
      this._postService.setPost(this.post).subscribe(
         response => {
            this.status = response.success.message;
            this.id = response.success.id;
            if(this.status == 'Post create successfully'){
               this._router.navigate(['post/'+this.id]);
            }
         },
         error =>{
            console.log(<any>error);
         }
      );
   }
}
