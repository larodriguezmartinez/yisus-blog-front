import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css'],
	providers: [ UserService ],
})
export class RegisterComponent implements OnInit {

	public pageTitle: string;
	public user: User;
	public status: string;

	constructor(
		private _userService: UserService
	){

		this.pageTitle = 'Registro';
		this.user = new User(null, '', '', '', '', '', 'avatar.png', 2, '');
	}

	ngOnInit() {
			console.log('%cInit REGISTER component', 'color: #bada55');
			// console.log(this._userService.test());
	}

	onSubmit(form){
		this._userService.register(this.user).subscribe(
			response => {
				this.status = response.success.message;
				form.reset();
			},
			error =>{
				console.log(<any>error);
			}
		);
	}
}

