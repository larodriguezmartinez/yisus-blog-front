import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { User } from '../../models/user';
import { PostService } from '../../services/posts.service';
import { global } from '../../services/global';
import { Router, ActivatedRoute, Params } from '@angular/router';


@Component({
   selector: 'app-post-detail',
   templateUrl: './post-detail.component.html',
   styleUrls: ['./post-detail.component.css'],
   providers: [ PostService ],
})

export class PostDetailComponent implements OnInit {

   public pageTitle: string;
   public url: string;
   public post: Post;
   public user: User;

   constructor(
      private _postService: PostService,
      private _route: ActivatedRoute,
      private _router: Router
   ) {

      this.url = global.url;
      this.pageTitle = "Post detail";
      console.log('%cInit POST DETALLE component', 'color: #bada55');
   }

   ngOnInit() {
      this.getPost();
      this.post = new Post(null,null,'','','',null,null);
      this.user = new User(null, '', '', '', '', '', '', null, '');
   }

   getPost(){

      this._route.params.subscribe(

         params => {
            let id = +params['id'];

            this._postService.getPost(id).subscribe(
               response => {
                  if(response.success.message == 'Post find'){
                     this.post = response.success.post;
                     this.user = response.success.post.user;
                  }else{
                     this._router.navigate(['/posts']);
                  }
               },
               error => {
                  this._router.navigate(['/inicio']);
                  console.log(<any>error);
               }
            );
         }
      );
   }

}
