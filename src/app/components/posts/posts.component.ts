import { Component, OnInit } from '@angular/core';
import { Post } from '../../models/post';
import { PostService } from '../../services/posts.service';
import { global } from '../../services/global';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
  providers: [ PostService ],
})

export class PostsComponent implements OnInit {

   public pageTitle: string;
   public url: string;
   public posts: Array<Post>;

   constructor(

      private _postService: PostService

   ) {
      this.url = global.url;
      this.pageTitle = "Últimos posts";
      console.log('%cInit POSTS component', 'color: #bada55');
   }

   ngOnInit() {
      this.getPosts();
   }

   getPosts(){
      this._postService.getPosts().subscribe(
         response => {
            if(response.success.message == 'Post list'){
               this.posts = response.success.posts;
            }
         },
         error => {
            console.log(<any>error);
         }
      );
   }

}
